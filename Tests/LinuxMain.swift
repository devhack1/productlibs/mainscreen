import XCTest

import MainScreenTests

var tests = [XCTestCaseEntry]()
tests += MainScreenTests.allTests()
XCTMain(tests)
