import XCTest
@testable import MainScreen

final class MainScreenTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MainScreen().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
