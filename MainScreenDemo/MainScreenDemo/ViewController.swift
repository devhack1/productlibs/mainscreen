//
//  ViewController.swift
//  MainScreenDemo
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit
import MainScreen

class MainNavigation: UINavigationController, MainScreenActions {
    func openCredit(accountModel: MainAccountModelProtocol) {
        showAlert(msg: #function + accountModel.name)
    }

    func openAccount(accountModel: MainAccountModelProtocol) {
        showAlert(msg: #function + accountModel.name)
    }

    func openCard(accountModel: MainAccountModelProtocol) {
        showAlert(msg: #function + accountModel.name)
    }

    func showAlert(msg: String) {
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.addAction(.init(title: "ok", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }


}

