//
//  SceneDelegate.swift
//  MainScreenDemo
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit
import MainScreen

struct Provider: MainScreenAccountProviderProtocol {
    func loadData(completion: @escaping ([MainAccountModelProtocol]) -> Void) {
        completion([MainScreenAccountModel(image: nil, name: "name", payment: "payment", amount: "amount", datePayment: "date", pincode: "pincode")])
    }
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = makeRootScreen()
            self.window = window
            window.makeKeyAndVisible()
        }
    }

    open func makeRootScreen() -> UIViewController {
        let nav = MainNavigation()
        let vc = MainScreenFactory().makeMainScreen(dataProvider: MainScreenDataProvider(creditsProvider: Provider(), cardsProvider: Provider(), accountsProvider: Provider()), actionInteractor: nav)
        nav.setViewControllers([vc], animated: false)
        return nav
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

