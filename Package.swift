// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MainScreen",
    platforms: [.iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "MainScreen",
            type: .dynamic,
            targets: ["MainScreen"]),
    ],
    dependencies: [
        .package(name: "DSKit", url: "https://gitlab.com/devhack1/level1/dskit", .upToNextMajor(from: "1.0.0")),
        .package(name: "KMPCore", url: "https://gitlab.com/devhack1/level0/kmpcore", .upToNextMajor(from: "1.0.0")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "MainScreen",
            dependencies: ["DSKit", "KMPCore"]),
        .testTarget(
            name: "MainScreenTests",
            dependencies: ["MainScreen"]),
    ]
)
