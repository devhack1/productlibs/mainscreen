//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import Foundation
import UIKit
import SnapKit
import DSKit
import KMPCore

public
class MainScreenViewController: UIViewController {

    weak var actionsInteractor: MainScreenActions?
    var dataProvider: MainScreenDataProvider?

    private var sectionName:[String] = ["Карты", "Счета", "Кредиты"]
    private var hiddenSections = Set<Int>()
    private var models: [[MainAccountModelProtocol]] = [] {
        didSet { tableView.reloadData() }
    }
    
    private var tableView: UITableView!
    
    private var sectionButton = BaseMainRowButton(state: .up, name: "Карты")

    public override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ThemeManager.shared.currentTheme.getColor(.backgroundPrimary)
        dataProvider?.loadFullData(completion: { (models) in
            self.models = models
        })
        setupView()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
    }
    
    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
        tableView.reloadData()
    }
    
}

private extension MainScreenViewController {
    private func setupView() {
        self.tableView = UITableView()
        tableView.attributeBackgroundColor = ColorAttribute.backgroundPrimary
        view.attributeBackgroundColor = ColorAttribute.backgroundPrimary
        tableView.separatorStyle = .none
        tableView.allowsSelection = true
        tableView.alwaysBounceVertical = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 56
        tableView.register(BaseMainCountCell.self, forCellReuseIdentifier: "\(BaseMainCountCell.self)")
        tableView.dataSource = self
        tableView.delegate = self
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    @objc
    private func hideSection(sender: BaseMainRowButton) {
        let section = sender.tag
        
        func indexPathsForSection() -> [IndexPath] {
            var indexPaths = [IndexPath]()
            
            for row in 0..<self.models[section].count {
                indexPaths.append(IndexPath(row: row,
                                            section: section))
            }
            
            return indexPaths
        }
        
        if self.hiddenSections.contains(section) {
            self.hiddenSections.remove(section)
            self.tableView.insertRows(at: indexPathsForSection(),
                                      with: .fade)
            sender.buttonAction()
        } else {
            self.hiddenSections.insert(section)
            self.tableView.deleteRows(at: indexPathsForSection(),
                                      with: .fade)
            sender.buttonAction()
        }
        
    }
}

extension MainScreenViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hiddenSections.contains(section) {
            return 0
        }
        
        return models[section].count
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.models.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BaseMainCountCell()
        let model = models[indexPath.section][indexPath.row]

        let color = ThemeManager.shared.currentTheme.getColor(.secondaryBlue) ?? .black
        let secondColor = ThemeManager.shared.currentTheme.getColor(.backgroundPrimary) ?? .black
        let first = model
            .name
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 18).font)
            .color(color)
        let second = model
            .payment
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 22).font)
            .color(color)
        let second1 = model
            .amount
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 22).font)
            .color(secondColor)
        second.append(second1)
        let third = model
            .datePayment
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 16).font)
            .color(color)
        let third1 = model
            .pincode
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 16).font)
            .color(color)
        third.append(third1)
        cell.configire(image: model.image, firstText: first, secondText: second, thirdText: third)
        return cell
    }
}

extension MainScreenViewController: UITableViewDelegate {

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0: actionsInteractor?.openCard(accountModel: models[indexPath.section][indexPath.row])
        case 1: actionsInteractor?.openAccount(accountModel: models[indexPath.section][indexPath.row])
        case 2: actionsInteractor?.openCredit(accountModel: models[indexPath.section][indexPath.row])
        default: break
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        sectionButton = BaseMainRowButton(state: .up, name: sectionName[section])
        sectionButton.attributeBackgroundColor = ColorAttribute.primaryHeader
        sectionButton.tag = section
        
        sectionButton.addTarget(self,
                                action: #selector(self.hideSection(sender:)),
                                for: .touchUpInside)
        let view = UIView()
        view.addSubview(sectionButton)
        sectionButton.snp.makeConstraints { make in
            make.top.equalTo(6)
            make.left.equalTo(CGFloat.leftInset)
            make.bottom.equalTo(-6)
            make.width.equalTo(180)
        }
        
        
        return view
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56
    }
    
}
