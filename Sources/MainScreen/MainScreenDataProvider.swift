//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import Foundation

public struct MainScreenDataProvider {
    var creditsProvider: MainScreenAccountProviderProtocol
    var cardsProvider: MainScreenAccountProviderProtocol
    var accountsProvider: MainScreenAccountProviderProtocol
    
    public init(
        creditsProvider: MainScreenAccountProviderProtocol,
        cardsProvider: MainScreenAccountProviderProtocol,
        accountsProvider: MainScreenAccountProviderProtocol
    ) {
        self.creditsProvider = creditsProvider
        self.cardsProvider = cardsProvider
        self.accountsProvider = accountsProvider
    }

    func loadFullData(completion: @escaping ([[MainAccountModelProtocol]]) -> Void) {
        var credits = [MainAccountModelProtocol]()
        var cards = [MainAccountModelProtocol]()
        var accounts = [MainAccountModelProtocol]()
        let group = DispatchGroup()
        group.enter()
        creditsProvider.loadData { (models) in
            credits = models
            group.leave()
        }
        group.enter()
        cardsProvider.loadData { (models) in
            cards = models
            group.leave()
        }
        group.enter()
        accountsProvider.loadData { (models) in
            accounts = models
            group.leave()
        }
        group.notify(queue: .main) {
            completion([cards, accounts, credits])
        }
    }
}
