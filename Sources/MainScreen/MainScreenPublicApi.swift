//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit

public protocol MainAccountModelProtocol {
    var image: UIImage? { get }
    var name: String { get }
    var payment: String { get }
    var amount: String { get }
    var datePayment: String { get }
    var pincode: String { get }
}

public struct MainScreenFactory {
    public init() {}
    public func makeMainScreen(
        dataProvider: MainScreenDataProvider,
        actionInteractor: MainScreenActions
    ) -> UIViewController {
        let vc = MainScreenViewController()
        vc.dataProvider = dataProvider
        vc.actionsInteractor = actionInteractor
        return vc
    }
}

public protocol MainScreenActions: AnyObject {
    func openCredit(accountModel: MainAccountModelProtocol)
    func openAccount(accountModel: MainAccountModelProtocol)
    func openCard(accountModel: MainAccountModelProtocol)
}

public protocol MainScreenAccountProviderProtocol {
    func loadData(completion: @escaping ([MainAccountModelProtocol]) -> Void)
}
