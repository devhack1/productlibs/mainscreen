//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import UIKit
import DSKit

public struct MainScreenAccountModel: MainAccountModelProtocol {
    public let image: UIImage?
    public let name: String
    public let payment: String
    public let amount: String
    public let datePayment: String
    public let pincode: String
    
    public init(
        image: UIImage? = nil,
        name: String? = "",
        payment: String? = "",
        amount: String? = "",
        datePayment: String? = "",
        pincode: String? = ""
    ) {
        self.image = image
        self.name = name ?? ""
        self.payment = payment ?? ""
        self.amount = amount ?? ""
        self.datePayment = datePayment ?? ""
        self.pincode = pincode ?? ""
    }
}
